import React, {useContext} from 'react';
import { Navigate } from 'react-router-dom';

import LogoEletrorastro from '../assets/image/logoEletrorastro.webp';
import { Button} from 'semantic-ui-react';
import Input from '../components/Input';
import useForm from '../Hooks/useForm';
import { UserContext } from '../UseContext';
import SendEmail from '../SendEmail';

import { 
  Container, 
  BackgroundLeftColor, 
  BackgroundRightColor, 
  Form,
  Space
} from './styles';


function Login() {

  const { userLogin, data, loading, login } = useContext(UserContext);

  const username = useForm();
  const password = useForm();

  async function handleSubmit(event) {
    event.preventDefault();

      if (username.validate() && password.validate()) {
        userLogin(username.value, password.value);

      }
  }

  if (login === true) {
    if(data.primeiro_acesso == 'F') {
      return <Navigate to="/AddSenha" />
    } else {
      return <Navigate to="/" />
    }
  }


  return (
      <Container>
        <BackgroundLeftColor />
        <BackgroundRightColor />
        <Form onSubmit={handleSubmit}>
            <img src={LogoEletrorastro} alt="Eletrorastro"/>
            <h2>Login</h2>
            <Input 
              label="Usuário" 
              name="username" 
              type="text"
              {...username}
            />
            <Input 
              label="Senha" 
              name="password" 
              type="password" 
              {...password}
            />
            <Space>
            {loading ? (
              <Button positive disabled>Carregando...</Button>
            ) : (
              <Button positive >Entrar</Button>
            )}
            <SendEmail/>
            </Space>
        </Form>
      </Container>
  );

}

export default Login;

